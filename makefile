FILENAME = flyer
TEXFLAGS := -pdf -xelatex --shell-escape

$FILENAME).pdf: $(FILENAME).tex Discord-Logo+Wordmark-Color.svg vt.svg
	latexmk $(TEXFLAGS) $(FILENAME).tex

watch:
	latexmk $(TEXFLAGS) -pvc $(FILENAME).tex

clean:
	latexmk -c

clean-all:
	latexmk -C
	$(RM) -r svg-inkscape $(FILENAME).xdv
